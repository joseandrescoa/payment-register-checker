import React, { useState } from 'react';
import Papa from 'papaparse';
import { utils, writeFile } from "xlsx";


const App = () => {
  const [data, setData] = useState([]);
  const [deuda, setDeuda] = useState([]);
  const [allData, setAllData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [number, setNumber] = useState(0);

  

  const handleFileUpload = (event) => {
    const file = event.target.files[0];
    setLoading(true);
    readFile(file);
  };

  const readFile = (file) => {
    setLoading(true);
    let arrayDeudas = [];
    let allDeudas = [];

    Papa.parse(file, {
      complete: (results) => {
        let read = results.data;
        read.map((e)=>{

          // Obtener todas als transcciones con identificador "FS USD"
          if(e[1]?.substr(0,6)=="FS USD") {

            // Obtener cuenta
            let text = e[1]
            let parts = text.split("|");
            let after = parts[1];
            let cuenta = after.trim()
            cuenta = parseInt(cuenta)

            // Obtener monto real
            let partes = text.split(" ");
            let value = partes[2];
            let montReal = value.split("|")
            montReal = parseFloat(montReal[0])/100

            // Obtener monto cobrado
            let cobrado = parseFloat(e[4])

            // Deficit
            let diferencia = Math.round((montReal-cobrado) * 100) / 100;


            if(montReal-cobrado != 0){

              // Guardo aparte las que tienen un deficit entre lo que se pago y lo que se debia pagar
              arrayDeudas.push({'cuenta':cuenta,'monto_cobrado':cobrado,'monto_real':montReal,'codigo':e[0],'deficit':diferencia});
            }

            // Guardo un registro de todas las Transacciones con identificador "FS USD"
            allDeudas.push({'cuenta':cuenta,'monto_cobrado':cobrado,'monto_real':montReal,'codigo':e[0],'deficit':diferencia});

          }
          
        })
        
        setLoading(false);
        setData(arrayDeudas)
        setAllData(allDeudas)
      },
    });
  };

  // Exportar EXCEL de resumen Global
  const exportToExcel = (allData) => {
    const ws = utils.json_to_sheet(allData);
    const wb = utils.book_new();
    utils.book_append_sheet(wb, ws, 'Sheet1');
    writeFile(wb, 'All_Transactions_FS_USD.xlsx');
  };

  // Exporta a Excel un resumen por banco
  const exportToExcelTBC = (data) => {
    let newCuentas = []
    data.map((e)=>{
  
      let cuentaExiste = false;
      for(var i=0; i<newCuentas.length; i++){
        if(newCuentas[i].cuenta == e.cuenta){
          cuentaExiste = true;

          newCuentas[i].deuda = Math.round((newCuentas[i].deuda + e.monto_real) * 100) / 100;
          newCuentas[i].pagado = Math.round((newCuentas[i].pagado + e.monto_cobrado) * 100) / 100;
          newCuentas[i].deficit = Math.round((newCuentas[i].deficit + e.deficit) * 100) / 100;

          break;
        }
      }
  
      if (!cuentaExiste) {
        newCuentas.push({'cuenta':e.cuenta,'pagado':e.monto_cobrado,'deuda':e.monto_real,'deficit': Math.round((e.deficit) * 100) / 100,'codigo':e.codigo})
      }
    });
    const ws = utils.json_to_sheet(newCuentas);
    const wb = utils.book_new();
    utils.book_append_sheet(wb, ws, 'Sheet1');
    writeFile(wb, 'Bad_Transactions_byCommerce.xlsx');
  };


  // Calculo de deuda total
  const exportDeudaTotal = (data) => {
    let deuda = 0;
    data.map((e)=>{
      deuda = Math.round((deuda + e.deficit) * 100) / 100;
    })
    setDeuda(deuda)
  };

  // Obtener nuemro de cuenta ingresado por el usuario
  const handleChange = (event) => {
    setNumber(event.target.value);
  };

  // Obtener el resumen para un comercio en especifico
  const exportResumenIndividual = (data,number) => {
    let newCuentas = []

    for(var i=0; i<data.length; i++){
      if(data[i].cuenta == number){
        newCuentas.push({'cuenta':data[i].cuenta,'pagado':data[i].monto_cobrado,'deuda':data[i].monto_real,'deficit': Math.round((data[i].deficit) * 100) / 100,'codigo':data[i].codigo})
      }
    }

    console.log(newCuentas);
    const ws = utils.json_to_sheet(newCuentas);
    const wb = utils.book_new();
    utils.book_append_sheet(wb, ws, 'Sheet1');
    writeFile(wb, 'Bad_Transactions_Single_Commerce.xlsx');
  }

  return (
    <div>
      <input type="file" onChange={handleFileUpload} />
      {loading ? (
        <div>Loading...</div>
      ) : (
        <>
        </>
      )}
      {data.length > 0 ?
        <div>
          <button onClick={() => exportToExcel(allData)}>Resumen de todas las trans. FS USD</button>
          <button onClick={() => exportToExcelTBC(data)}>Resumen por comercio</button>
          <button onClick={() => exportDeudaTotal(data)}>Deuda Total</button>
          {deuda>0?<div>{'Deuda Global: '+deuda}</div>:null}
          <input type="number" value={number} onChange={handleChange} />
          <button onClick={() => exportResumenIndividual(data,number)}>Resumen de un comercio en especificco</button>
        </div>:null
      }
    </div>
  );
};

export default App;
